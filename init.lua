--[[
disbiome mod
by nac
Version: alpha-0.1
License: GNU General Public License v3.0
]]

disbiome = {
  mod = "disbiome",
  version = "alpha-0.1",
  path = minetest.get_modpath("disbiome"),
}

local modpath = core.get_modpath("disbiome")

minetest.unregister_biome("grassland")
minetest.unregister_biome("grassland_dunes")
minetest.unregister_biome("grassland_ocean")
minetest.unregister_biome("grassland_under")
minetest.unregister_biome("cold_desert")
minetest.unregister_biome("cold_desert_ocean")
minetest.unregister_biome("cold_desert_under")
minetest.unregister_biome("coniferous_forest")
minetest.unregister_biome("coniferous_forest_dunes")
minetest.unregister_biome("coniferous_forest_ocean")
minetest.unregister_biome("coniferous_forest_under")
minetest.unregister_biome("deciduous_forest")
minetest.unregister_biome("deciduous_forest_ocean")
minetest.unregister_biome("deciduous_forest_shore")
minetest.unregister_biome("deciduous_forest_under")
minetest.unregister_biome("desert")
minetest.unregister_biome("desert_ocean")
minetest.unregister_biome("desert_under")
minetest.unregister_biome("icesheet")
minetest.unregister_biome("icesheet_ocean")
minetest.unregister_biome("icesheet_under")
minetest.unregister_biome("sandstone_desert")
minetest.unregister_biome("sandstone_desert_ocean")
minetest.unregister_biome("sandstone_desert_under")
minetest.unregister_biome("savanna")
minetest.unregister_biome("savanna_ocean")
minetest.unregister_biome("savanna_shore")
minetest.unregister_biome("savanna_under")
minetest.unregister_biome("snowy_grassland")
minetest.unregister_biome("snowy_grassland_ocean")
minetest.unregister_biome("snowy_grassland_under")
minetest.unregister_biome("taiga")
minetest.unregister_biome("taiga_ocean")
minetest.unregister_biome("taiga_under")
minetest.unregister_biome("tundra")
minetest.unregister_biome("tundra_beach")
minetest.unregister_biome("tundra_highland")
minetest.unregister_biome("tundra_ocean")
minetest.unregister_biome("tundra_under")
